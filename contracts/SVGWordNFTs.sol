// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.12;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

import { Base64 } from "./libraries/Base64.sol";

contract SVGWordNFTs is ERC721URIStorage {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    string svgPartOne = "<svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMinYMin meet' viewBox='0 0 350 350'><style>.base { fill: white; font-family: serif; font-size: 24px; }</style><rect width='100%' height='100%' fill='";
    string svgPartTwo = "'/><text x='50%' y='50%' class='base' dominant-baseline='middle' text-anchor='middle'>";

    string[] colors = ["red", "black", "yellow", "blue", "green", "silver", "purple", "orange"];
    string[] firstWords = ["Funny", "Goofy", "Windy", "Silly", "Hungry", "Speedy", "Rich", "Broke", "Beany", "Old", "Young", "Skinny", "Obese", "Sleepy", "Stormy", "Smoky"];
    string[] secondWords = ["German", "Armenian", "British", "American", "Mexican", "Chinese", "French", "Polish", "Russian", "Indian", "Swedish", "Italian", "Kenyan", "Australian", "Brazilian", "Irish"];
    string[] thirdWords = ["Andrew", "Beans", "Brian", "Bryan", "Corey", "Ed", "Jonny", "Ken", "Matt", "Paul", "Phil", "Remy", "Rob", "Teej", "Wes", "Will"];

    event NewSVGWordNFTMinted(address sender, uint256 tokenId);

    constructor() ERC721 ("ChoppySeas", "YCHTWK") {
    }

    function random() private view returns (uint256) {
        return uint256(keccak256(abi.encodePacked(block.difficulty, block.timestamp)));
    }

    function pickRandomColor() public view returns (string memory) {
        uint256 randIndex = random() % colors.length;
        return colors[randIndex];
    }

    function pickRandomFirstWord() public view returns (string memory) {
        uint256 randIndex = random() % firstWords.length;
        return firstWords[randIndex];
    }

    function pickRandomSecondWord() public view returns (string memory) {
        uint256 randIndex = random() % secondWords.length;
        return secondWords[randIndex];
    }

    function pickRandomThirdWord() public view returns (string memory) {
        uint256 randIndex = random() % thirdWords.length;
        return thirdWords[randIndex];
    }

    function getTotalNFTsMintedSoFar() public view returns (uint256) {
        return _tokenIds.current();
    }

    function mintAnSVGWordNFT() public {
        uint256 newItemId = _tokenIds.current();
        require(newItemId < 1000, "Only 1000 NFTs can be minted.");
        string memory first = pickRandomFirstWord();
        string memory second = pickRandomSecondWord();
        string memory third = pickRandomThirdWord();
        string memory combinedWord = string(abi.encodePacked(first, second, third));
        string memory randomColor = pickRandomColor();
        string memory finalSvg = string(abi.encodePacked(svgPartOne, randomColor, svgPartTwo, combinedWord, "</text></svg>"));
        string memory json = Base64.encode(
            bytes(
                string(
                    abi.encodePacked(
                        '{"name": "',
                        combinedWord,
                        '", "description": "Highbrow art to peruse after a California Sick Day.", "image": "data:image/svg+xml;base64,',
                        Base64.encode(bytes(finalSvg)),
                        '"}'
                    )
                )
            )
        );
        string memory finalTokenUri = string(abi.encodePacked("data:application/json;base64,", json));
        _safeMint(msg.sender, newItemId);
        _setTokenURI(newItemId, finalTokenUri);
        _tokenIds.increment();
        emit NewSVGWordNFTMinted(msg.sender, newItemId);
    }
}