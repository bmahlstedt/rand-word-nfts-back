const main = async () => {
  const nftContractFactory = await hre.ethers.getContractFactory('SVGWordNFTs');
  const nftContract = await nftContractFactory.deploy();
  await nftContract.deployed();
  console.log("Contract deployed to:", nftContract.address);
  // Tests below
  let txn = await nftContract.mintAnSVGWordNFT()
  let total = await nftContract.getTotalNFTsMintedSoFar()
  await txn.wait()
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch(error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();