const main = async () => {
  const nftContractFactory = await hre.ethers.getContractFactory('SVGWordNFTs');
  const nftContract = await nftContractFactory.deploy();
  await nftContract.deployed();
  console.log("Contract deployed to:", nftContract.address);
  // Mint one NFT on production deploy so that we can grab the OpenSea
  // collection URL easily.
  let txn = await nftContract.mintAnSVGWordNFT({ gasLimit: 10000000 });
  await txn.wait()
  console.log("Minted one NFT to generate OpenSea collection link (may take 1min or so to appear in UI)");
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch(error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();