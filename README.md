Needs a `.env` file with:
```
ALCHEMY_KEY=
PRIVATE_KEY=
ETHERSCAN_KEY=
```

To test, run:
```
npx hardhat run scripts/test.js
```

To deploy, run:
```
npx hardhat run scripts/deploy.js --network rinkeby
```

You'll need the contract address, artifact, and OpenSea collection URL to copy over to the dapp frontend.

Verify the contract on etherscan:
```
npx hardhat verify <contractAddress> --network rinkeby
```